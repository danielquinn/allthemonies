#!/usr/bin/env python3

from __future__ import annotations

import argparse
import re

from datetime import datetime
from pathlib import Path
from typing import Iterable

import pandas as pd
import pandas_datareader.data as web
import plotly.express as px


class Bank:

    CURRENCY = None
    PREFIX = None
    NAME: str

    NAME_REGEX = re.compile("([^-]+)-(.*)")

    def __init__(self, path: Path):

        self.path = path

        self.name = self.NAME
        if m := self.NAME_REGEX.match(path.stem):
            self.name += f" ({m.group(2)})"

    def __str__(self):
        return self.name

    @classmethod
    def get_available(cls) -> Iterable[Bank]:
        for subclass in cls.__subclasses__():
            root = Path(".")
            for path in root.glob(f"{subclass.PREFIX}*.csv"):
                yield subclass(path)

    def get(self, target_currency: str) -> pd.DataFrame:
        return self._sanitise(self._build(), target_currency)

    def _build(self) -> pd.DataFrame:
        raise NotImplementedError()

    def _sanitise(self, df: pd.DataFrame, target: str) -> pd.DataFrame:

        df = df.resample("D").last().ffill()

        if self.CURRENCY == target:
            return df

        forex = Forex(self.CURRENCY, target)
        df["Balance"] = pd.Series(
            df["Balance"]
            .reset_index()
            .apply(
                lambda _: _.loc["Balance"]
                * forex[str(_.Date.date())]["Close"],
                axis=1,
            )
            .values,
            index=df["Balance"].index,
        )

        return df


class NatWest(Bank):

    CURRENCY = "GBP"
    PREFIX = "natwest"
    NAME = "Natwest"

    def _build(self) -> pd.DataFrame:
        return pd.read_csv(
            self.path,
            names=(
                "Date",
                "Type",
                "Description",
                "Value",
                "Balance",
                "Account Name",
                "Account Number",
                "_",
            ),
            header=0,
            usecols=("Date", "Balance"),
            parse_dates=["Date"],
            date_parser=lambda date: datetime.strptime(date, "%d/%m/%Y"),
            index_col="Date",
        )


class Vancity(Bank):

    CURRENCY = "CAD"
    PREFIX = "vancity"
    NAME = "Vancity"

    def _build(self) -> pd.DataFrame:
        return pd.read_csv(
            self.path,
            names=(
                "Account Number",
                "Date",
                "Description",
                "_",
                "Debit",
                "Credit",
                "Balance",
            ),
            usecols=("Date", "Balance"),
            parse_dates=["Date"],
            index_col="Date",
        )


class TriodosUK(Bank):

    CURRENCY = "GBP"
    PREFIX = "triodosuk"
    NAME = "Triodos UK"

    def _build(self) -> pd.DataFrame:
        df = pd.read_csv(
            self.path,
            names=(
                "Date",
                "Sort Code",
                "Account Number",
                "Delta",
                "Code",
                "Description",
                "_",
            ),
            converters={
                "Delta": lambda x: float(x.replace(",", "")),
            },
            usecols=("Date", "Delta"),
            parse_dates=["Date"],
            date_parser=lambda date: datetime.strptime(date, "%d/%m/%Y"),
            index_col="Date",
        )
        df["Balance"] = df["Delta"].cumsum()
        del df["Delta"]
        return df


class TriodosNL(Bank):

    CURRENCY = "EUR"
    PREFIX = "triodosnl"
    NAME = "Triodos NL"

    def _build(self) -> pd.DataFrame:
        df = pd.read_csv(
            self.path,
            names=(
                "Date",
                "IBAN",
                "Delta",
                "Type",
                "Name",
                "Other Account",
                "Code",
                "Reference",
            ),
            converters={
                "Delta": lambda x: float(x.replace(".", "").replace(",", ".")),
                "Type": lambda _: {"Credit": 1, "Debet": -1}[_],
            },
            usecols=("Date", "Delta", "Type"),
            parse_dates=["Date"],
            date_parser=lambda date: datetime.strptime(date, "%d-%m-%Y"),
            index_col="Date",
        )
        df["Delta"] *= df["Type"]
        df["Balance"] = df["Delta"].cumsum()
        del df["Delta"]
        del df["Type"]
        return df


class Forex:

    CACHE_TEMPLATE = ".forex-cache/{}{}.json"
    CACHE_TIME = 3  # Hours

    def __init__(self, currency_from: str, currency_to: str):
        self.currency_from = currency_from
        self.currency_to = currency_to
        self.cache_path = Path(
            self.CACHE_TEMPLATE.format(self.currency_from, self.currency_to)
        )

        self.data = self._get_data()

    def __getitem__(self, item):
        return self.data.loc[item]

    def _get_data(self) -> pd.DataFrame:

        try:
            now = datetime.now()
            stats = self.cache_path.stat().st_mtime
            updated_at = datetime.fromtimestamp(stats)
            if (now - updated_at).seconds / 60 / 60 < self.CACHE_TIME:
                with self.cache_path.open() as f:
                    return pd.read_json(f.read())
        except FileNotFoundError:
            pass

        df = self._fetch()
        with self.cache_path.open("w") as f:
            f.write(df.to_json())

        return df

    def _fetch(self) -> pd.DataFrame:
        print(f"Fetching {self.currency_from}:{self.currency_to} from Yahoo!")
        return (
            web.DataReader(
                f"{self.currency_from}{self.currency_to}=X",
                "yahoo",
                datetime(2012, 1, 1).date(),
                datetime.now().date(),
            )
            .resample("D")
            .last()
            .ffill()
        )


class Command:
    def __init__(self):

        self.banks = list(Bank.get_available())

        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--currency",
            action="store",
            default="EUR",
            help="The currency in which you want all accounts displayed",
        )

        self.args = parser.parse_args()

        self.currency = self.args.currency

    def main(self):
        df = pd.concat([b.get(self.currency) for b in self.banks], axis=1)
        df.columns = [b.name for b in self.banks]
        px.area(
            df.resample("D").last().ffill(),
            title="All The Monies",
        ).show()


if __name__ == "__main__":
    Command().main()
