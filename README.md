# allthemonies

A means of keeping tabs on where my money is.


## Why

I have money in a bunch of different accounts and currencies and when I move stuff around, it's not always easy to get a handle on my current financial standing.  This is an attempt to remedy that.


## How it Works

Banks are old and stupid, so any dream I had of writing code to talk to a secure NatWest API and pull down my transaction history were dashed pretty early in this process.  No, you still have to do the "talking to the bank" process manually: go to your bank, download your transactions in a CSV format saved in a file with a specific naming convetion, and then run the script

```bash
$ ls
allthemonies  data.json  LICENSE  natwest.csv  poetry.lock  pyproject.toml  README.md  static  triodosnl-current.csv  triodosnl-savings.csv  triodosuk-current.csv  triodosuk-isa.csv  vancity.csv
$ ./allthemonies
```

This will take a minute to compile the data and fetch any necessary FOREX values (if you're storing different currencies), then it'll just open your browser to show a fancy chart.

If you'd like the output rendered in a currency other than Euros, you can pass that on the command line:

```bash
$ ./allthemonies --currency CAD
```


### Naming Convetion

It'll look for files named `natwest.csv`, `triodosuk.csv`, `triodosnl.csv` and `vancity.csv` with an option to include an arbitrary additional name, like `triodosuk-isa.csv` to refer to my ISA account with Triodos or `natwest-superawesomefunaccount.csv` for some other reason.


## What it Supports

Currently, I've got accounts at Vancity, Triodos (NL), Triodos (UK), and NatWest, so that's what's supported.  If you fork this to support your own institution(s), please issue a pull request so I can add more.
